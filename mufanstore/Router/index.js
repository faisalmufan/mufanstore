import React from "react";
import { StyleSheet, Text, View } from "react-native";
import Homescreen from "../screens/Homescreen";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";
import Welcomescreen from '../screens/Welcomescreen'
import Loginscreen from '../screens/Loginscreen'
import Registerscreen from '../screens/Registerscreen'
import Aboutscreen from "../screens/Aboutscreen"
import Detailscreen from '../screens/Detailscreen'
import Forgotscreen from '../screens/Forgotscreen'
import Favoritscreen from '../screens/Favoritscreen'
import Cartscreen from '../screens/Cartscreen'
import Chattingscreen from '../screens/Chattingscreen'
import Profilescreen from '../screens/Profilescreen'
import { Feather } from '@expo/vector-icons';
import Ionicons from 'react-native-vector-icons/Ionicons';

const Tab = createBottomTabNavigator();
const Drawwer = createDrawerNavigator();
const Stack = createStackNavigator();

export default function Router() {
	return (
		<NavigationContainer>
			<Stack.Navigator>
				<Stack.Screen options={{headerShown: false}} name="Welcome" component={Welcomescreen}/>
				<Stack.Screen options={{title: "", headerStyle: {backgroundColor: "#9BDBFF"}, headerTransparent: true}} name="Loginscreen" component={Loginscreen}/>
				<Stack.Screen options={{title: "", headerTransparent: true}} name="Registerscreen" component={Registerscreen}/>
				<Stack.Screen options={{title: "", headerTransparent: true}} name="Detailscreen" component={Detailscreen}/>
				<Stack.Screen options={{title: "", headerTransparent: true}} name="Forgotscreen" component={Forgotscreen}/>
				<Stack.Screen options={{headerShown: false}} name="MainApp" component={MainApp}/>
			</Stack.Navigator>
		</NavigationContainer>
	)
}




const MainApp = () => (
		<Tab.Navigator
			screenOptions={({ route }) => ({
			tabBarIcon: ({ focused, color, size }) => {
				let iconName;

				if (route.name === 'Homescreen') {
					iconName = focused
					? 'ios-home'
					: 'ios-home-outline';
				} else if (route.name === 'Favoritscreen') {
					iconName = focused ? 'ios-heart' : 'ios-heart-outline';
				} else if (route.name === 'Cartscreen') {
					iconName = focused ? 'ios-cart' : 'ios-cart-outline';
				} else if (route.name === 'Aboutscreen') {
					iconName = focused ? 'ios-person' : 'ios-person-outline';
				} else if (route.name === 'Chattingscreen') {
					iconName = focused ? 'ios-chatbox' : 'ios-chatbox-outline';
				}

				// You can return any component that you like here!
				return <Ionicons name={iconName} size={size} color={color} />;
			},
			tabBarActiveTintColor: '#9BDBFF',
			tabBarInactiveTintColor: 'gray',
			})}
		>
			<Tab.Screen options={{tabBarShowLabel: false, headerShown: false}} name="Homescreen" component={Homescreen}/>
			<Tab.Screen options={{tabBarShowLabel: false, headerShown: false}} name="Favoritscreen" component={Favoritscreen}/>
			<Tab.Screen options={{tabBarShowLabel: false, headerShown: false}} name="Cartscreen" component={Cartscreen}/>
			<Tab.Screen options={{tabBarShowLabel: false, headerShown: false}} name="Chattingscreen" component={Chattingscreen}/>
			<Tab.Screen options={{tabBarShowLabel: false, headerShown: false}} name="Aboutscreen" component={Aboutscreen}/>
		</Tab.Navigator>
)

// const MyDrawwer = () => (
// 	<Drawwer.Navigator screenOptions={{ headerShown: false }}>
// 		<Drawwer.Screen name="App" component={MainApp} />
// 		<Drawwer.Screen name="AboutScreen" component={AboutScreen}/>
// 	</Drawwer.Navigator>
// )