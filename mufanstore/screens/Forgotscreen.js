import { StatusBar, StyleSheet, Text, View, TextInput, TouchableOpacity, ScrollView } from 'react-native'
import React, {useEffect, useState} from 'react'
import { useFonts } from 'expo-font'
import { getAuth, sendPasswordResetEmail } from "firebase/auth";


export default function Forgotscreen ({navigation}) {
  const [email, setEmail] = useState("")
  const [isError, setIsError] = useState(false)

//Signin
  const onResetPassword = () => {
    const auth = getAuth();
    sendPasswordResetEmail(auth, email)
      .then(() => {
        // Password reset email sent!
        // ..
      })
      .catch((error) => {
        const errorCode = error.code;
        const errorMessage = error.message;
        // ..
      });
  }

  const [loaded] = useFonts({
    MontserratRegular: require('../assets/fonts/Montserrat-Regular.otf'),
    MontserratBold: require('../assets/fonts/Montserrat-Bold.otf')
  });
  
  if (!loaded) {
    return null;
  }
  return (
    <>
    <StatusBar translucent={false}></StatusBar>
    <ScrollView style={{width: "100%"}}>
    <View style={styles.container}>
      <Text style={styles.title}>Forgot Password</Text>
      <Text style={styles.titleForm}>Reset</Text>
      <TextInput 
        style={styles.textInput} 
        placeholder="Enter your email address"
        value={email}
        onChangeText={(value) => setEmail(value)} />
      <Text style={styles.textSend}>*We will you send a message to reset your password !!</Text>
      <TouchableOpacity style={styles.buttonResetPassword}>
        <Text style={styles.textResetPassword} onPress={onResetPassword}>Reset</Text>
      </TouchableOpacity>
    </View>
    </ScrollView>
    </>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "80%",
    alignSelf: 'center'
  },

  title: {
    marginTop: 115,
    fontSize: 27,
    color: "#444444",
    fontFamily: "MontserratBold",
    marginBottom: 50,
    alignSelf: 'center',
  },

  titleForm: {
    fontSize: 27,
    color: "#444444",
    fontFamily: "MontserratBold",
    marginBottom: 50,
    alignSelf: "flex-start",
  },

  textInput: {
    borderBottomWidth: 1,
    borderBottomColor: "#9BDBFF",
    height: 50,
  },

  textSend: {
    marginTop: 10,
    marginBottom: 50,
    fontSize: 11,
    fontFamily: "MontserratRegular",
  },

  buttonResetPassword: {
    height: 57,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 11,
    backgroundColor: "#9BDBFF",
  },

  textResetPassword: {
    fontSize: 21,
    color: "#444444",
    fontFamily: "MontserratBold",
  }
})