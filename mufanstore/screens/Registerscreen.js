import { StatusBar, StyleSheet, Text, View, TextInput, TouchableOpacity, ScrollView } from 'react-native'
import React, { useState } from 'react'
import { useFonts } from 'expo-font'
import { getAuth, createUserWithEmailAndPassword } from "firebase/auth";


export default function Registerscreen ({navigation}) {

  const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");
	const [isError, setIsError] = useState(false);

//Signup New User
  const onSignedUp = () => {
    const auth = getAuth();
    if (confirmPassword===password) {
      createUserWithEmailAndPassword(auth, email, password)
        .then((userCredential) => {
          // Signed in 
          const user = userCredential.user;
          console.log(`Registered with : ${user.email}`);
          alert('Registered is success !!');
          navigation.navigate('Loginscreen');
          setEmail("");
          setPassword("");
          setConfirmPassword("");
        })
        .catch((error) => {
          const errorCode = error.code;
          const errorMessage = error.message;
          alert(errorMessage)
          console.log(errorCode, errorMessage);
        });
    } else {
      alert("Password not match !!");
    }
  }

  const [loaded] = useFonts({
    MontserratRegular: require('../assets/fonts/Montserrat-Regular.otf'),
    MontserratBold: require('../assets/fonts/Montserrat-Bold.otf')
  });
  
  if (!loaded) {
    return null;
  } 
  return (
    <>
    <StatusBar translucent={false}></StatusBar>
    <ScrollView style={{width: "100%"}}>
    <View style={styles.container}>
      <Text style={styles.title}>Let's Start</Text>
      <Text style={styles.titleForm}>Sign up</Text>
      <View>
        <TextInput 
          style={styles.textInput} 
          placeholder="Email/username"
					value={email}
					onChangeText={(value) => setEmail(value)} ></TextInput>
        <TextInput 
          style={styles.textInput} 
          placeholder="Password" 
          secureTextEntry 
					value={password}
					onChangeText={(value) => setPassword(value)}></TextInput>
        <TextInput 
          style={styles.textInput} 
          placeholder="Confirm Password" 
          secureTextEntry
					value={confirmPassword}
					onChangeText={(value) => setConfirmPassword(value)}></TextInput>
      </View>
      <TouchableOpacity>
        <Text style={styles.textForgot} onPress={() => navigation.navigate('Loginscreen')}>Already have an account?</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.buttonSign}>
        <Text style={styles.textSignin} onPress={onSignedUp}>Sign up</Text>
      </TouchableOpacity>
    </View>
    </ScrollView>
    </>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "80%",
    alignSelf: 'center'
  },

  title: {
    marginTop: 115,
    fontSize: 27,
    color: "#444444",
    fontFamily: "MontserratBold",
    marginBottom: 50,
    alignSelf: 'center',
  },

  titleForm: {
    fontSize: 27,
    color: "#444444",
    fontFamily: "MontserratBold",
    marginBottom: 50,
    alignSelf: "flex-start",
  },

  textInput: {
    borderBottomWidth: 1,
    borderBottomColor: "#9BDBFF",
    height: 50,
    marginBottom: 50,
  },

  textForgot: {
    fontSize: 11,
    color: "#444444",
    fontFamily: "MontserratBold",
    marginBottom: 15,
    alignSelf: "flex-end",
  },

  buttonSign: {
    height: 57,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 11,
    backgroundColor: "#9BDBFF",
  },

  textSignin: {
    fontSize: 21,
    color: "#444444",
    fontFamily: "MontserratBold",
  }
})