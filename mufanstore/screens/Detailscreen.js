import { StyleSheet, Text, View, Image } from 'react-native'
import React from 'react'
import { useFonts } from 'expo-font';

const Detailscreen = () => {
  // const {title} = route.params;

  const [loaded] = useFonts({
    MontserratRegular: require('../assets/fonts/Montserrat-Regular.otf'),
    MontserratBold: require('../assets/fonts/Montserrat-Bold.otf')
  });
  
  if (!loaded) {
    return null;
  }

  return (
    <>
    <View style={styles.container}>
      <View style={styles.imageContainer}>
        <Image style={styles.image} source={require('../assets/mufanlogo.png')}></Image>
      </View>
      <View style={styles.descriptionContainer}>
        <Text style={styles.textTitle}>Title : </Text>
        <Text style={styles.textBody}>title bae</Text>
        <Text style={styles.textTitle}>Price : </Text>
        <Text style={styles.textBody}>content for price</Text>
        <Text style={styles.textTitle}>Desription : </Text>
        <Text style={styles.textBody}>content for desription</Text>
        <Text style={styles.textTitle}>Category : </Text>
        <Text style={styles.textBody}>content for category</Text>
        <Text style={styles.textTitle}>Rating : </Text>
        <Text style={styles.textBody}>content for rate</Text>
      </View>
    </View>
    </>
  )
}

export default Detailscreen

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },

  imageContainer: {
    height: 250,
    width: 250,
    marginVertical: 60,
    elevation: 5,
    backgroundColor: "white",
    borderRadius: 11,
    justifyContent: 'center',
  },

  image: {
    borderWidth: 1,
    alignSelf: 'center',
  },

  descriptionContainer: {
    flex: 1,
    alignSelf: 'flex-start',
    marginLeft: 20,
  },

  textTitle: {
    fontFamily: "MontserratBold",
    fontSize: 13,
  },

  textBody: {
    fontFamily: "MontserratRegular",
    fontSize: 11,
    marginTop: 5,
    marginBottom: 20,
  }
})