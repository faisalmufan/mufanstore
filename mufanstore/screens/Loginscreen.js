import { StatusBar, StyleSheet, Text, View, TextInput, TouchableOpacity, ScrollView } from 'react-native'
import React, {useEffect, useState} from 'react'
import { useFonts } from 'expo-font'
import { getAuth, signInWithEmailAndPassword } from "firebase/auth";


export default function Loginscreen ({navigation}) {
  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")
  const [isError, setIsError] = useState(false)

//Signin
  const onSignedIn = () => {
    const auth = getAuth();
    signInWithEmailAndPassword(auth, email, password)
      .then((userCredential) => {
        // Signed in 
        const user = userCredential.user;
        console.log(`Login with : ${user.email}`);
        navigation.navigate('MainApp', {
          screen: 'Homescreen',
          params: email,
        });
      })
      .catch((error) => {
        const errorCode = error.code;
        const errorMessage = error.message;
        alert(errorCode);
        console.log(errorCode, errorMessage);
      });
  }

  const [loaded] = useFonts({
    MontserratRegular: require('../assets/fonts/Montserrat-Regular.otf'),
    MontserratBold: require('../assets/fonts/Montserrat-Bold.otf')
  });
  
  if (!loaded) {
    return null;
  }
  return (
    <>
    <StatusBar translucent={false}></StatusBar>
    <ScrollView style={{width: "100%"}}>
    <View style={styles.container}>
      <Text style={styles.title}>Welcome Back</Text>
      <Text style={styles.titleForm}>Sign in</Text>
      <TextInput 
        style={styles.textInput} 
        placeholder="Email/username"
        value={email}
        onChangeText={(value) => setEmail(value)} />
      <TextInput 
        style={styles.textInput} 
        placeholder="Password" 
        value={password}
        onChangeText={(value) => setPassword(value)}
        secureTextEntry />
      <TouchableOpacity>
        <Text style={styles.textForgot} onPress={() => navigation.navigate('Forgotscreen')}>Forgot password?</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.buttonSign}>
        <Text style={styles.textSignin} onPress={onSignedIn}>Sign in</Text>
      </TouchableOpacity>
    </View>
    </ScrollView>
    </>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "80%",
    alignSelf: 'center'
  },

  title: {
    marginTop: 115,
    fontSize: 27,
    color: "#444444",
    fontFamily: "MontserratBold",
    marginBottom: 50,
    alignSelf: 'center',
  },

  titleForm: {
    fontSize: 27,
    color: "#444444",
    fontFamily: "MontserratBold",
    marginBottom: 50,
    alignSelf: "flex-start",
  },

  textInput: {
    borderBottomWidth: 1,
    borderBottomColor: "#9BDBFF",
    height: 50,
    marginBottom: 50,
  },

  textForgot: {
    fontSize: 13,
    color: "#444444",
    fontFamily: "MontserratBold",
    marginBottom: 15,
    alignSelf: "flex-end",
  },

  buttonSign: {
    height: 57,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 11,
    backgroundColor: "#9BDBFF",
  },

  textSignin: {
    fontSize: 21,
    color: "#444444",
    fontFamily: "MontserratBold",
  }
})