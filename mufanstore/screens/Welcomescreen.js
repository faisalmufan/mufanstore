import { Button, StyleSheet, Text, View, Image, TouchableOpacity, StatusBar } from 'react-native'
import React from 'react'
import { useFonts } from 'expo-font'


export default function Welcomescreen({navigation}){
  const [loaded] = useFonts({
    MontserratRegular: require('../assets/fonts/Montserrat-Regular.otf'),
    MontserratBold: require('../assets/fonts/Montserrat-Bold.otf')
  });
  
  if (!loaded) {
    return null;
  }
  return(
    <>
    <StatusBar translucent={false}></StatusBar>
    <View style={styles.container}>
      <View style={styles.logoContainer}>
        <Image style={styles.imageLogo} source={require("../assets/mufanlogo.png")}></Image>
      </View>
      <View style={styles.textContainer}>
        <Text style={styles.textTitle}>Welcome</Text>
        <Text style={styles.text}>{`Selamat Datang di Toko MuFan, \ndapatkan kemudahan \nuntuk memenuhi kebutuhan anda.\nApa yang anda butuhkan dapat kami sediakan, \nkebutuhan anda adalah \nprioritas kami dalam melayani.`}</Text>
      </View>
      <View style={styles.buttonContainer}>
      <TouchableOpacity style={styles.buttonSignin}>
        <Text style={styles.textSignin} onPress={() => navigation.navigate('Loginscreen')}>Sign in</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.buttonSignup}>
        <Text style={styles.textSignup} onPress={() => navigation.navigate('Registerscreen')}>Sign up</Text>
      </TouchableOpacity>
      </View>
    </View>
    </>
  )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center', 
    },

    logoContainer: {
      height: 411,
      width: 320,
      backgroundColor: "#9BDBFF",
      borderRadius: 25,
      marginVertical: 30,
      justifyContent: 'center',
    },

    imageLogo: {
      height: 231,
      width: 231,
      alignSelf: 'center',
    },

    textContainer: {
      marginHorizontal: 24,
    },

    textTitle: {
      fontSize: 21,
      alignSelf: 'center',
      fontWeight: 'bold',
      fontFamily: 'MontserratBold',
      marginBottom: 10,
    },

    text: {
      fontSize: 11,
      fontFamily: "MontserratRegular",
      textAlign: 'center',
      marginBottom: 25,
    },

    buttonContainer: {
      flexDirection: 'row',
      height: 57,
      width: 300,
    },
    
    buttonSignin: {
      flex: 1,
      backgroundColor: "#9BDBFF",
      justifyContent: 'center',
      padding: 10,
      alignItems: 'center',
      borderTopLeftRadius: 11,
      borderBottomLeftRadius: 11,
      borderWidth: 1,
      borderColor: "#9BDBFF",
    },

    textSignin: {
      fontFamily: "MontserratBold",
      color: "white",
    },

    buttonSignup: {
      flex: 1,
      justifyContent: 'center',
      padding: 10,
      alignItems: 'center',
      borderColor: "#9BDBFF",
      borderWidth: 1,
      borderTopRightRadius: 11,
      borderBottomRightRadius: 11,
      borderLeftWidth: 0,
    },

    textSignup: {
      fontFamily: "MontserratBold",
      color: "#444444",
    }
})