import { StyleSheet, Text, View, Image, StatusBar } from 'react-native'
import React from 'react'
import { useFonts } from 'expo-font'
import { ScrollView } from 'react-native-gesture-handler';

const Aboutscreen = () => {
  const [loaded] = useFonts({
    MontserratRegular: require('../assets/fonts/Montserrat-Regular.otf'),
    MontserratBold: require('../assets/fonts/Montserrat-Bold.otf')
  });
  
  if (!loaded) {
    return null;
  }
  return (
    <>
    <StatusBar translucent={false}></StatusBar>
    <ScrollView>
    <View style={styles.container}>
      <View style={styles.topContainer}>
        <View style={styles.about}>
          <Text style={styles.textTitleBold}>{`About \nDeveloper`}</Text>
          <Text style={styles.textTitlReg}>Muchammad</Text>
          <View style={{flexDirection: 'row'}}>
            <Text  style={styles.textTitleBold}>Faisal</Text>
            <Text style={styles.textTitlReg}> Nurdin</Text>
          </View>
            <Text style={styles.fromKocir}>from Kota Cirebon</Text>
        </View>
        <View>
          <Image style={styles.image} source={require('../assets/faisal.jpg')}></Image>
        </View>
      </View>
      <View style={styles.midContainer}>
        <View style={styles.icon}>
          <Image style={styles.spaceIcon} source={require('../assets/icons/gitlab.png')}></Image>
          <Image style={styles.spaceIcon} source={require('../assets/icons/ig.png')}></Image>
          <Image style={styles.spaceIcon} source={require('../assets/icons/tw.png')}></Image>
        </View>
        <Text style={styles.textMedsos}>@faisalmufan</Text>
      </View>
      <View style={styles.bottomContainer}>
        <Text style={styles.textTitleBottom}>Bahasa Pemrograman</Text>
        <Text style={styles.textBottom}>{`\u2022 Javascript (basic)`}</Text>
        <Text style={styles.textBottom}>{`\u2022 PHP (intermediate)`}</Text>
        <Text style={styles.textBottom}>{`\u2022 CSS (intermediate)`}</Text>
        <Text style={styles.textBottom}>{`\u2022 HTML (intermediate)`}</Text>
        <Text style={styles.textTitleBottom}>Framework</Text>
        <Text style={styles.textBottom}>{`\u2022 React Native (basic)`}</Text>
        <Text style={styles.textBottom}>{`\u2022 Codeigniter (intermediate)`}</Text>
        <Text style={styles.textTitleBottom}>Portofolio</Text>
        <Text style={styles.textBottom}>{`\u2022 sakti.cirebonkota.go.id`}</Text>
        <Text style={styles.textBottom}>{`\u2022 kecharjamukti.cirebonkota.go.id`}</Text>
        <Text style={styles.textTitleBottom}>Softskill</Text>
        <Text style={styles.textBottom}>{`\u2022 Mudah belajar`}</Text>
        <Text style={styles.textBottom}>{`\u2022 Mudah beradaptasi`}</Text>
        <Text style={styles.textBottom}>{`\u2022 Jujur`}</Text>
        <Text style={styles.textBottom}>{`\u2022 Berani`}</Text>
        <Text style={styles.textBottom}>{`\u2022 Sopan Santun`}</Text>
        <Text style={styles.textTitleBottom}>Teknologi</Text>
        <Text style={styles.textBottom}>{`\u2022 Figma, Vscode, Expo, Gitlab`}</Text>
        <Text style={styles.textBottom}>{`\u2022 Photoshop, Coreldraw`}</Text>
      </View>
    </View>
    </ScrollView>
    </>
  )
}

export default Aboutscreen

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },

  topContainer: {
    flexDirection: 'row',
    marginHorizontal: 25,
    marginBottom: 17,
    marginTop: 30,
    height: 197, 
  },

  about: {
    flex: 1,
  },

  image: {
    flex: 1,
    borderWidth: 1,
    justifyContent: 'flex-end',
    width: 143,
    borderRadius: 25,
  },

  textTitlReg: {
    fontFamily: "MontserratRegular",
    fontSize: 21,
  },

  textTitleBold: {
    fontFamily: "MontserratBold",
    fontSize: 20,
    marginBottom: 20,
  },

  fromKocir: {
    marginTop: -20, 
    fontFamily: "MontserratRegular", 
    fontSize: 11,
  },

  midContainer: {
    height: 50,
    width: 340,
    flexDirection: 'row',
    backgroundColor: "#9BDBFF",
    borderTopLeftRadius: 11,
    borderBottomLeftRadius: 11,
  },

  icon: {
    flex: 1,
    flexDirection: 'row',
    alignSelf: 'center',
    marginLeft: 10,
  },

  textMedsos: {
    fontFamily: "MontserratRegular", 
    fontSize: 13,
    marginRight: 10,
    alignSelf: 'center'
  },

  spaceIcon: {
    margin: 5,
  },

  bottomContainer: {
    flex: 1,
    width: 340,
    marginTop: 10,
  },

  textTitleBottom: {
    fontFamily: "MontserratBold", 
    fontSize: 15,
    marginVertical: 5,
  },

  textBottom: {
    fontFamily: "MontserratRegular", 
    fontSize: 12,
  }

})