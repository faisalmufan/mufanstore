import { FlatList, Image, StatusBar, StyleSheet, Text, TextInput, View, TouchableOpacity, TouchableWithoutFeedback ,Modal, Pressable, Alert } from 'react-native'
import React, { useState, useEffect } from 'react'
import { async } from '@firebase/util';
import { useFonts } from 'expo-font';
import { Feather } from '@expo/vector-icons';
import { Button, Portal, Provider } from 'react-native-paper';
import auth, { firebase } from '@react-native-firebase/auth';
import signOut from '@react-native-firebase/auth';

const Homescreen = ({route, navigation}) => {
  // const {email} = route.params;
  const [isLoading, setIsLoading] = useState(true);
  const [data, setData] = useState([]);
  const [modalVisible, setModalVisible] = useState(false);

  const signOutUser = async () => {
      try {
          await firebase.auth().signOut();
          navigation.navigate('Welcomescreen');
      } catch (e) {
          console.log(e);
      }
  }

  const onSignout = () => {
    // auth()
    //   .signOut()
    //   .then(() => console.log('User signed out!'));
      navigation.navigate('Loginscreen');
  }

  const getProducts = async () => {
    try {
      fetch('https://fakestoreapi.com/products?limit=20')
            .then(res=>res.json())
            .then(json=>setData(json))
    } catch (error) {
      console.log(error);
    } finally {
      setIsLoading(false)
    }
  }

  useEffect(() => {
    getProducts();
  }, [])

  // const onDetail = () => {
  //   navigation.navigate('Detailscreen',
  //   {data});
  // }
  
  const [loaded] = useFonts({
    MontserratRegular: require('../assets/fonts/Montserrat-Regular.otf'),
    MontserratBold: require('../assets/fonts/Montserrat-Bold.otf')
  });
  
  if (!loaded) {
    return null;
  }

  return (
    <>
    <StatusBar translucent={false}/>
    <Provider>
      <Portal>
        <Modal
        transparent={true}
        visible={modalVisible}
        onDismiss={()=> setModalVisible(false)} 
        >
            <View style={styles.topContainer}>
              <View style={styles.modalView}>
                <TouchableOpacity 
                onPress={onSignout}
                >
                  <Text style={styles.modalText}>Sign out</Text>
                </TouchableOpacity>
              </View>
            </View>
        </Modal>
      </Portal>
    <View style={styles.container}>
      <View style={styles.topContainer}>
          <Text style={styles.textTitleProducts}>Hai, Faisal</Text>
          <Pressable
            onPress={() => setModalVisible(true)}>
            <Image style={styles.imageProfile} source={require('../assets/faisal.jpg')}></Image>
          </Pressable>
      </View>
      <View style={styles.searchContainer}>
        <TextInput
          style={styles.searchInput}
          placeholder='Search'
        />
        <TouchableOpacity style={styles.buttonSearch}>
          <Image source={require('../assets/icons/search.png')}></Image>
        </TouchableOpacity>
      </View>
      <View style={styles.productContainer}>
        <FlatList
          numColumns={2}
          data={data}
          keyExtractor={({id}, index) => id}
          renderItem={({item}) => (
            <>
              <View style={styles.contentContainer}>
                <Text style={styles.textTitleProducts}>{item.category}</Text>
                <Text style={styles.textPrice}>${item.price}</Text>
                <Image style={styles.imageProducts} source={{uri: item.image}}/>
                <View style={styles.buttonContainer}>
                  <TouchableOpacity style={styles.buttonAdd}>
                    <Text style={styles.textButtonAdd}>Add</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={styles.buttonDetail}>
                    <Text style={styles.textButtonDetail} onPress={() => navigation.navigate('Detailscreen')}>Detail</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </>
          )}
          />
      </View>
    </View>
    </Provider>
    </>
  )
}

export default Homescreen

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  topContainer: {
    marginTop: 27,
    alignSelf: 'flex-end',
    marginRight: 20,
    flexDirection: 'row',
  },

  imageProfile: {
    height: 30,
    width: 30,
    marginLeft: 10,
    borderRadius: 5,
  },

  searchContainer: {
    flexDirection: 'row',
    marginTop: 20,
    marginHorizontal: 20,
    borderRadius: 11,
    height: 42,
    justifyContent: 'space-between',
    paddingLeft: 10,
    elevation: 10,
    backgroundColor: "white",
  },

  searchInput: {
    flex: 1,
  },

  buttonSearch: {
    justifyContent: 'center',
    height: 42,
    padding: 10,
  },

  productContainer: {
    flex: 1,
    margin: 20,
    alignItems: 'center',
  },

  contentContainer: {
    alignItems: 'center',
    width: 165,
    backgroundColor: "white",
    elevation: 5,
    margin: 5,
    borderRadius: 11,
    paddingVertical: 10,
  },

  imageProducts: {
    height: 100,
    width: 100,
    resizeMode: 'contain',
    marginBottom: 10,
  },

  textTitleProducts: {
    fontFamily: "MontserratBold",
    fontSize: 11,
    textAlign: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },

  textPrice: {
    fontFamily: "MontserratRegular",
    fontSize: 11,
    marginBottom: 10,
  },

  buttonContainer: {
    flexDirection: "row",
    width: 110,
    height: 25,
    justifyContent: 'center',
    borderRadius: 11,
  },

  buttonAdd: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: "#9BDBFF",
    borderBottomLeftRadius: 11,
    borderTopLeftRadius: 11,
    justifyContent: 'center',
  },

  buttonDetail: {
    flex: 1,
    alignItems: 'center',
    borderColor: "#9BDBFF",
    borderWidth: 1,
    borderBottomRightRadius: 11,
    borderTopRightRadius: 11,
    justifyContent: 'center',
  },

  textButtonAdd: {
    fontSize: 11,
    fontFamily: "MontserratBold",
    color: "white",
  },

  textButtonDetail: {
    fontSize: 11,
    fontFamily: "MontserratBold",
    color: "#444444",
  },

  modalView: {
    backgroundColor: "white",
    elevation: 5,
    borderRadius: 5,
    marginTop: 30,
    padding: 10,
  },

  modalText: {
    textAlign: 'right',
    fontFamily: "MontserratRegular",
    fontSize: 11,
  },
})