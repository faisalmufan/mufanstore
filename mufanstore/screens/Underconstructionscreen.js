import { Button, StyleSheet, Text, View, Image, TouchableOpacity, StatusBar } from 'react-native'
import React from 'react'
import { useFonts } from 'expo-font'


export default function Underconstructionscreen({navigation}){
  const [loaded] = useFonts({
    MontserratRegular: require('../assets/fonts/Montserrat-Regular.otf'),
    MontserratBold: require('../assets/fonts/Montserrat-Bold.otf')
  });
  
  if (!loaded) {
    return null;
  }
  return(
    <>
    <StatusBar translucent={false}></StatusBar>
    <View style={styles.container}>
      <View style={styles.logoContainer}>
        <Image style={styles.imageLogo} source={require("../assets/mufanlogo.png")}></Image>
      </View>
      <View style={styles.botContainer}>
        <View style={styles.hoalContainer}></View>
        <View style={styles.contectContainer}>
            <Text style={styles.textUc}>{`Sorry, this page is \nunder construction`}</Text>
            <View style={styles.imageContainer}>
                <Image style={styles.imageEngineer} source={require('../assets/engineer.png')}></Image>
                <Image style={styles.imageUc} source={require('../assets/uc.png')}></Image>
            </View>
        </View>
      </View>
    </View>
    </>
  )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center', 
    },

    logoContainer: {
      height: 380,
      width: 270,
      backgroundColor: "#9BDBFF",
      borderRadius: 25,
      marginVertical: 30,
      justifyContent: 'center',
    },

    imageLogo: {
      height: 231,
      width: 231,
      alignSelf: 'center',
    },

    botContainer: {
        borderWidth: 5,
        borderTopWidth: 0,
        marginTop: -65,
        borderRadius: 25, 
        height: 380,
        width: 330,
        borderColor: "#9BDBFF",
    },

    hoalContainer: {
        borderWidth: 5,
        height: 40,
        borderRadius: 25,
        borderColor: "#9BDBFF",
        marginBottom: 70,
    },

    contectContainer: {
        alignItems: 'center',
    },

    textUc: {
        fontSize: 20,
        fontFamily: "MontserratBold",
        textAlign: 'center',
        marginBottom: 50,
        color: "#444444",
    },

    imageContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderColor: "#9BDBFF",
    },

    imageEngineer: {
        height: 110,
        width: 110,
    },
    
    imageUc: {
        width: 110,
        height: 110,
    }
})