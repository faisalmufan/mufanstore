import { StyleSheet, Text, View } from "react-native";
import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import Welcomescreen from '../screens/Welcomescreen';
import Homescreen from '../screens/Homescreen';
import Loginscreen from "../screens/Loginscreen";
import Registerscreen from "../screens/Registerscreen";
import { NavigationContainer } from "@react-navigation/native";

const Stack = createStackNavigator();

export const index = () => {
	return (
        <NavigationContainer>
            <Stack.Navigator>
                {/* <Stack.Screen name="Welcomescreen" component={Welcomescreen} /> */}
                <Stack.Screen name="Loginscreen" component={Loginscreen} />
                <Stack.Screen name="Registerscreen" component={Registerscreen} />
            </Stack.Navigator>
        </NavigationContainer>
	);
};

const styles = StyleSheet.create({});
