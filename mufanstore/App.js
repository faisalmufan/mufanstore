import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import React, {useState, useEffect} from 'react';
import Router from './Router'
  // Import the functions you need from the SDKs you need
import { getApps, initializeApp } from "firebase/app";
import Homescreen from './screens/Homescreen';
import Newscreen from './screens/newscreen';
import Detailscreen from './screens/Detailscreen';
import Underconstructionscreen from './screens/Underconstructionscreen';
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

export default function App() {

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyA5Ly31jfgrV7X85G8BbOde14N6O8vDM74",
  authDomain: "mufanstore-75eb5.firebaseapp.com",
  projectId: "mufanstore-75eb5",
  storageBucket: "mufanstore-75eb5.appspot.com",
  messagingSenderId: "309524684172",
  appId: "1:309524684172:web:c04cdcac393528d36a0aa9"
};

// Initialize Firebase
if (!getApps.length) {
  initializeApp(firebaseConfig);
}

  return (
    <Router/>
    // <Homescreen/>
    // <Detailscreen/>
    // <Newscreen/>
    // <Underconstructionscreen/>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  }
});
